package com.ay.hib.hib_tutor;

import com.ay.hib.hib_tutor.entities.Book;
import com.ay.hib.hib_tutor.services.BookService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.sql.Date;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HibTutorApplicationTests {

    @Autowired
    private BookService bookService;

    @Test
    public void contextLoads() {
        Book book = new Book();
        String id = "9781484201282";
        book.setIsbn(id);
        book.setName("Hibernate Recipes");
        book.setPrice(new BigDecimal("44.00"));
        book.setPublishdate(Date.valueOf("2014-10-10"));

        bookService.create(book);

        Book book1 = bookService.getById(id);
        assertEquals(book.getName(), book1.getName());
        book1 = bookService.getById(id);
        assertEquals(book.getName(), book1.getName());
    }

}
