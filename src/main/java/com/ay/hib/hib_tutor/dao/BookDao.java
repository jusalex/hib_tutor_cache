package com.ay.hib.hib_tutor.dao;

import com.ay.hib.hib_tutor.entities.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by ayurtaev on 19.08.17.
 */
@Repository
public interface BookDao extends CrudRepository<Book, String> {
}
