package com.ay.hib.hib_tutor.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by ayurtaev on 19.08.17.
 */
@Entity
public class Publisher implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private String code;

    @Column
    private String name;

    @Column
    private String address;

    public Publisher() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
