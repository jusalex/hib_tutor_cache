package com.ay.hib.hib_tutor;

import com.ay.hib.hib_tutor.config.DBConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.ay.hib.hib_tutor"})
@EnableAutoConfiguration
public class HibTutorApplication {

    public static void main(String[] args) {
        SpringApplication.run(new Object[]{
                HibTutorApplication.class, DBConfig.class}, args);
    }
}
