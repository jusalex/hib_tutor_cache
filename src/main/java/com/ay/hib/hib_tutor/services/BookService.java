package com.ay.hib.hib_tutor.services;

import com.ay.hib.hib_tutor.dao.BookDao;
import com.ay.hib.hib_tutor.entities.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * Created by ayurtaev on 19.08.17.
 */
@Service
public class BookService {

    @Autowired
    private BookDao bookDao;

    public Book create(Book book) {
        return bookDao.save(book);
    }

    @Transactional(readOnly = true)
    @Cacheable("bookFindCache")
    public Book getById(String id) {
        slowLookupOperation();
        return bookDao.findOne(id);
    }

    public void slowLookupOperation() {
        try {
            long time = 5000L;
            Thread.sleep(time);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }

}
